﻿namespace ShapeNamespace
{
    class Circle : Shape
    {
        private double radius;

        public Circle(double radius)
        {
            this.radius = radius;
        }

        public override double CalculateArea()
        {
            return Math.PI * radius * radius;//radius binh phuong
        }

        public override double CalculatePerimeter()
        {
            return 2 * Math.PI * radius;//radius *2
        }
    }
}
