﻿using System;

namespace ShapeNamespace
{

    class Program
    {
        static void Main(string[] args)
        {
            try
            {

                Shape[] shapes = new Shape[9];
                shapes[0] = new Rectangle(5, 10);
                shapes[1] = new Rectangle(7.5, 4.5);
                shapes[2] = new Rectangle(6.8, 9.3);
                shapes[3] = new Triangle(3, 4, 5);
                shapes[4] = new Triangle(6, 8, 10);
                shapes[5] = new Triangle(7.5, 9.6, 12.4);
                shapes[6] = new Circle(5);
                shapes[7] = new Circle(7.5);
                shapes[8] = new Circle(10);

                for (int i = 0; i < shapes.Length; i++)
                {
                    Console.WriteLine("The area of the " + shapes[i].GetType().Name + " is " + shapes[i].CalculateArea());
                    Console.WriteLine("The perimeter of the " + shapes[i].GetType().Name + " is " + shapes[i].CalculatePerimeter());
                    Console.WriteLine();
                }

                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine( e.Message );

            }
            
           

        }
    }
}
